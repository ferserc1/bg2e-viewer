
#include <event_handler.hpp>

template <class T>
class FindComponentVisitor : public bg::scene::NodeVisitor {
public:
	typedef std::vector<T*> ResultVector;

	inline void clear() { _result.clear(); }

	inline ResultVector & result() { return _result; }
	inline const ResultVector & result() const { return _result; }

	void visit(bg::scene::Node * node) {
		T * comp = node->component<T>();
		if (comp) {
			_result.push_back(comp);
		}
	}

protected:
	ResultVector _result;
};

MainEventHandler::MainEventHandler(const std::string & initScene)
	:_sceneName(initScene)
{
	
}

MainEventHandler::~MainEventHandler() {
	
}

void MainEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else {
		throw bg::base::CompatibilityException("No such suitable rendering engine.");
	}
}

bool MainEventHandler::openScene(const std::string & scene) {
	bg::ptr<bg::scene::Node> sceneRoot = bg::db::loadScene(context(), scene);
	if (sceneRoot.valid()) {
		_sceneRoot = sceneRoot.getPtr();

		bg::ptr<FindComponentVisitor<bg::scene::Camera>> findCamera = new FindComponentVisitor<bg::scene::Camera>();
		_sceneRoot->accept(findCamera.getPtr());
		if (findCamera->result().size() > 0) {
			for (auto c : findCamera->result()) {
				setupCameraNode(c->node());
				break;
			}
		}
		else {
			bg::scene::Node * cameraNode = new bg::scene::Node(context());
			cameraNode->addComponent(new bg::scene::Camera());
			setupCameraNode(cameraNode);
		}
		return true;
	}
	return false;
}

void MainEventHandler::setupCameraNode(bg::scene::Node * cameraNode) {
	using namespace bg::scene;
	using namespace bg::math;

	_camera = cameraNode->camera();

	cameraNode->addComponent(new Transform());
	bg::manipulation::OrbitNodeController * orbit = new bg::manipulation::OrbitNodeController;
	cameraNode->addComponent(orbit);
	bg::ptr<OpticalProjectionStrategy> projection = new OpticalProjectionStrategy;
	projection->setFocalLength(Scalar(35, distance::mm));
	_camera->setProjectionStrategy(projection.getPtr());

	orbit->setDistance(Scalar(5, distance::meter));
	orbit->setRotation(Vector2(Scalar(-22.5, trigonometry::deg),
		Scalar(45.0f, trigonometry::deg)));

}

void MainEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());
	_renderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathDeferred);

	bg::db::DrawableLoader::RegisterPlugin(new bg::db::plugin::ReadDrawableBg2());
	bg::db::NodeLoader::RegisterPlugin(new bg::db::plugin::ReadPrefabBg2());
	bg::db::NodeLoader::RegisterPlugin(new bg::db::plugin::ReadScene());

	{
		using namespace bg::scene;
		using namespace bg::math;

		if (_sceneName.empty() || !openScene(_sceneName)) {
			_sceneRoot = new Node(context());

			Node * cameraNode = new Node(context(), "sceneRoot");
			_sceneRoot->addChild(cameraNode);
			cameraNode->addComponent(new Camera());
			setupCameraNode(cameraNode);

			Node * lightNode = new Node(context());
			_sceneRoot->addChild(lightNode);
			Transform * trx = new Transform();
			trx->matrix()
				.rotate(Scalar(95, trigonometry::deg), -1.0f, 0.0f, 0.0f)
				.rotate(Scalar(45, trigonometry::deg), 0.0f, 1.0f, 0.0f);
			lightNode->addComponent(trx);
			lightNode->addComponent(new bg::scene::Light);
		}
		
		_inputVisitor = new bg::scene::InputVisitor;
	}
}

void MainEventHandler::willDestoryContext() {
	
}

void MainEventHandler::destroy() {
	
}

void MainEventHandler::reshape(int w, int h) {
	_windowSize.set(w, h);
	_camera->setViewport(bg::math::Viewport(0, 0, w, h));
}

void MainEventHandler::frame(float delta) {
	_renderer->frame(_sceneRoot.getPtr(), delta);
}

void MainEventHandler::draw() {
	_renderer->draw(_sceneRoot.getPtr(), _camera.getPtr());
	context()->swapBuffers();
}

void MainEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	_inputVisitor->keyUp(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::keyDown(const bg::base::KeyboardEvent & evt) {
	_inputVisitor->keyDown(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::charPress(const bg::base::KeyboardEvent & evt) {
	_inputVisitor->charPress(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDown(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDrag(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::mouseMove(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseMove(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::mouseUp(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseUp(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::mouseWheel(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseWheel(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::onMemoryWarning() {
}

void MainEventHandler::touchStart(const bg::base::TouchEvent & evt) {
	_inputVisitor->touchStart(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::touchMove(const bg::base::TouchEvent & evt) {
	_inputVisitor->touchMove(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::touchEnd(const bg::base::TouchEvent & evt) {
	_inputVisitor->touchEnd(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::sensorEvent(const bg::base::SensorEvent & evt) {
	_inputVisitor->sensorEvent(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::buildMenu(bg::wnd::MenuDescriptor & menu) {
	using namespace bg::wnd;
	bg::ptr<PopUpMenu> fileMenu = PopUpMenu::New("File");
	fileMenu->addMenuItem({ 0, "Open" });
	fileMenu->addMenuItem({ 1, "Quit" });

	menu.push_back(fileMenu.getPtr());
}

void MainEventHandler::menuSelected(const std::string & title, int32_t identifier) {
	switch (identifier) {
	case 0: {
			bg::ptr<bg::wnd::FileDialog> fileDialog = bg::wnd::FileDialog::OpenFileDialog();
			fileDialog->addFilter("vitscnj");
			fileDialog->addFilter("vitscn");

			if (fileDialog->show() && this->openScene(fileDialog->getResultPath())) {
				this->reshape(_windowSize.width(), _windowSize.height());
			}
		}
		break;
	case 1:
		bg::wnd::MainLoop::Get()->quit(0);
		break;
	}
}
