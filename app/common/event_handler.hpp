
#ifndef _event_handler_hpp_
#define _event_handler_hpp_

#include <bg/bg2e.hpp>

class MainEventHandler : public bg::base::EventHandler {
public:
	MainEventHandler(const std::string & initScene);
	
	void willCreateContext();
	
	// Common events
	void initGL();
	void reshape(int,int);
	void frame(float);
	void draw();
	void willDestoryContext();
	void destroy();

	// Desktop events
	void keyUp(const bg::base::KeyboardEvent &);
	void keyDown(const bg::base::KeyboardEvent &);
	void charPress(const bg::base::KeyboardEvent &);
	void mouseDown(const bg::base::MouseEvent &);
	void mouseDrag(const bg::base::MouseEvent &);
	void mouseMove(const bg::base::MouseEvent &);
	void mouseUp(const bg::base::MouseEvent &);
	void mouseWheel(const bg::base::MouseEvent &);

	// Mobile devices events
	void onMemoryWarning();
	void touchStart(const bg::base::TouchEvent &);
	void touchMove(const bg::base::TouchEvent &);
	void touchEnd(const bg::base::TouchEvent &);
	void sensorEvent(const bg::base::SensorEvent &);

	void buildMenu(bg::wnd::MenuDescriptor & menu);
	void menuSelected(const std::string & title, int32_t identifier);

protected:
	virtual ~MainEventHandler();
	
	bool openScene(const std::string & scene);

	void setupCameraNode(bg::scene::Node *);

	bg::ptr<bg::render::Renderer> _renderer;
	bg::ptr<bg::scene::Node> _sceneRoot;
	bg::ptr<bg::scene::Camera> _camera;
	bg::ptr<bg::scene::InputVisitor> _inputVisitor;
	bg::ptr<bg::wnd::PopUpMenu> _menu;
	std::string _sceneName;
	bg::math::Size2Di _windowSize;
};

#endif
