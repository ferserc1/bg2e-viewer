
#include <event_handler.hpp>

#include <iostream>

#ifdef _WIN32
#include <Windows.h>
#endif

#ifdef _WIN32
int CALLBACK WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow) {
	int argc = 1;
	char * argv[] = { "", lpCmdLine };
	char * argv1 = GetCommandLineA();
	if (strlen(lpCmdLine)>0) {
		argc = 2;
	}
#else
int main(int argc, char ** argv) {
#endif
	{
		bg::ptr<bg::wnd::Window> window = bg::wnd::Window::New();
		std::string scene = "";
		if (argc > 1) {
			scene = argv[1];
		}

		if (!window.valid()) {
			std::cerr << "Platform not supported" << std::endl;
			return -1;
		}
		
		window->setRect(bg::math::Rect(50,50,800,600));
		window->setTitle("bg2e viewer");
		window->setEventHandler(new MainEventHandler(scene));
		window->create();
		
		bg::wnd::MainLoop::Get()->setWindow(window.getPtr());
	}
	return bg::wnd::MainLoop::Get()->run();
}
